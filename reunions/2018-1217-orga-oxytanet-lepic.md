# Réu Oxytanet - Le PIC

Présents : Tomj, Nim, Victor, Manu, Squeeek, Numahell

## Faire aboutir le cloud

Intégration LDAP ? possible, mais pas nécessaire.

Compte admin à 1 personne, qui crée des comptes pour son asso

Et si un user appartient à plusieurs asso ?
cloisonner par groupe ? 

Ça pose la question de fédération, d'identité sur internet.

commencer par quelque chose de simple. 

C'est quoi l'identifiant ? adresse mail ou user ?

User qui ont des comptes. La notion des groupes se fait au niveau des services (cloud, pad, …) ?

Pour commencer : userjohn@le-pic.org, userjohn@autre-asso.net

Technos qui peuvent le faire, OpenId, Oauth… comment on l'intègre ?
Et comment on gère la création de compte, si une asso crée un compte pour quelqu'un qui a déjà un compte ?

Faisons 1 asso qui gère ses comptes utilisateurs.

Keycloack ? utile pour avoir le même id pour gérer cloud, pad etc.

On part sur :

- 1 compte admin par asso
- ce compte admin gère les comptes de son asso

Nextcloud est multidomaine.


### Serveur

Serveur spécifique pour ces services.

Serveur de test ? non car pas assez puissant pour une prod. Besoin de 2VCPU et 4Go de Ram mini

Coût adapté, "à la main". Un bon serveur, peut-être 50€, mais le budget du Pic est très faible.
Voir au niveau prix libre, se mettre d'accord.

Prix libre ? bien être au fait du travail que ça demande, pas seulement des ressources matérielles, et à qui ça rend service et quelles sont ses ressources.

Tetaneutral propose quelques services non rémunérés, comme peertube, nos oignons, etc.

On peut imaginer avoir un serveur pour évaluer, et qu'à l'AG on ait des billes à proposer : est-ce qu'on fait payer ceux qui ont plus de moyens ?

Flexibilité de Tetaneutral sert à ça : on peut proposer le service pendant 3-6 mois, et on discute ensuite du prix. Risque de se retrouver avec un coût plus élevé qu'attendu, mais on peut quand même commencer avec un serveur peu coûteux, et quelques utilisateurs.

Stockage : plutôt de l'investissement, le PIC peut acheter un disque.

ex nos oignons, 

Décision : mise en place d'un service Beta, avoir un point de départ pour avoir des informations de base pour discuter en AG.

Beta : 4-5 utilisateurs actifs, + des gens qui téléchargent.

Aujourd'hui tetaneutral a 50Go d'avance, on a de la marge mais on peut monitorer.

### Config

Un serveur qui héberge keycloak, cloud et pad.
- backup : config au moins ?
- monitoring ?

Un dépôt dédié pour le Pic pour le déploiement. 
Indépendant de l'infra existante du Pic.

Config : 1VCPU + 4Go
pas encore nécessaire d'avoir 2VCPU, c'est une Beta, les utilisateurs feront leurs retours.

Monitoring ? des outils: portainer pour gérer les conteneurs dans une interface web, sentry pour la remontée d'erreurs, mais pas tout à fait pour du monitoring.

AG de janvier ? qui vient ? 29/01 Nim ok

framadate à envoyer → Tom

### Wishlist

Dernière question de Luc Novales (transmise par Manue) : Lime survey ? créer une issue sur le dépôt docker-atelier avec le tag correspondant pour qu'on travaille sur son intégration.

### Backups ?

Sur le Ceph de tetaneutral, devrait pas avoir de pb. Mais si on fait des conneries ?

- fichiers data pas backup (beta)
- mais config et DB oui

Dervich sur le Pic : script perl → dump puis scipt qui récupère les fichiers

Nextcloud recommande de mettre le site en maintenance pour effectuer le backup, mais pas sûr que ce soit nécessaire pour juste la DB.

### Plan d'action

Atelier déploiement en prod : quand ?

Doc création de compte pour les assos.

MyPads : nom, prénom, email

Changer thème keycloak

Date atelier ? à partir du 7/01 ¬ Tom lance un framadate.

## Qu'est-ce que Oxytanet ?

Mettre en contact club info INSA pour présentation (d'ailleurs nim présente à l'N7 club info)

Nous ne sommes pas un chatons : plutôt un groupe de travail, technique mais pas que, qui fournit des briques techniques pour les chatons, et éventuellement peut accompagner des chatons.

Idée de Squeeek : gamelle et pelote. Sinon, croquettes, herbe à chat ?

"Démeleur de pelote", on aime bien.

On motive des chatons, on fait grandir les chatons, éleveurs de chatons.

Limite à l'Occitanie ? ou pas seulement ?

Pas forcément de l'accompagnement, qui prend du temps.

Pas seulement public assos

À réfléchir plus avant.

Non technique ?
- formation
- réflexion sur nos actions
- aspects éthique
- orienté utilisateurs
- groupe d'intérêt, ayant les mêmes besoins

Fin à 21h environ